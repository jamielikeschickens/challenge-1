#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*
 *	Uses an n-ary tree represented where node have a pointer to the first child and other children are
 *	accessed through the use of the next pointer
 *	
 *  Nodes next to each other should be AND'd
 *  OR node indicates that's it's children and the next OR node (next to it) should be OR'd together
 *  usually the children of OR nodes may have next items meaning them items should be AND'd and then
 *	overall OR'd or they could just be single variables
 */

typedef struct logic_node {
	char name; // Name of variable
	int isNOT; // Whether variable should be NOT'd
	int isConstant; // Whether constant TRUE or FALSE, 1/0 stored in char
	int isOR; // If node is indicated as being OR children should be evaluated and OR'd 
	struct logic_node *next;
	struct logic_node *children;
} logic_node;

logic_node *create_node();
void createTree(logic_node **root_node, char *input);
int check_brackets(char *input);
void printTree();

int main(int argc, char *argv[]) {
	char input[BUFSIZ];
	fgets(input, BUFSIZ, stdin);
	logic_node *root_node;
    
    if (check_brackets(input) != 0) {
        printf("input error (unmatched brackets)\n");
    } else {
	
        createTree(&root_node, input);
    
        printTree(root_node);
        printf("\n");
    }
    return 0;
}

// 0 is valid, -1 invalid
int check_brackets(char *input) {
    int open_count = 0;
    int close_count = 0;
    
    for (int i=0; i < strlen(input); ++i) {
        if (input[i] == '(') {
            ++open_count;
        } else if (input[i] == ')') {
            ++close_count;
        }
    }
    
    if (open_count == close_count) {
        return 0;
    } else {
        return -1;
    }
}

void createTree(logic_node **root_node, char *input) {
	logic_node *current_node;
	logic_node *first_child_node;
    logic_node *current_child_node;

	int first_child_node_set = 0;
	int root_node_set = 0;
    int expression_has_finished = 0;
    int second_or_term_started = 0;
    int first_or_should_be_set_as_root = 0;
    
    // If there's no brackets but there is an or the first or should be set as root
    for (int i=0; i < strlen(input); ++i) {
        static int bracket_found = 0;
        if (input[i] =='(') {
            bracket_found = 1;
        } else if (input[i] == ' ') {
            if (bracket_found == 0) {
                first_or_should_be_set_as_root = 1;
            }
        }
    }

	int i = 0;
	char c;
	for (i=0; i<strlen(input); ++i) {
		c = input[i];
		if (c != ' ' && c != '\n' && c != ')') {
			// We are dealing with a character
			logic_node *var_node = create_node();
			var_node->name = c;
			if (c >= 'A' && c<='Z') {
				// Variable is capital and so should be NOT'd
				var_node->isNOT = 1;
			} else {
				if (c == '1' || c == '0') {
					var_node->isConstant = 1;
				} else {
					var_node->isConstant = 0;
				}
				var_node->isNOT = 0;
			}
			if (root_node_set == 0 && first_or_should_be_set_as_root == 0 && second_or_term_started == 0) {
				*root_node = var_node;
				current_node = var_node;
                root_node_set = 1;
            } else if (first_child_node_set == 0) {
                first_child_node = var_node;
                current_child_node = var_node;
                first_child_node_set = 1;
            } else {
                current_child_node->next = var_node;
                current_child_node = var_node;
            }
		} else if (c == ' ') {
			// Else must be OR node
			logic_node *or_node = create_node();
			or_node->isOR = 1;
			or_node->children = first_child_node;	
			if (first_or_should_be_set_as_root == 1) {
				*root_node = or_node;
				current_node = or_node;
                or_node->children = first_child_node;
                
                first_child_node_set = 0;
                first_child_node = NULL;
                current_child_node = NULL;
                first_or_should_be_set_as_root = 0;
                root_node_set = 1;
			} else {
				current_node->next = or_node;
				current_node = or_node;
			}
            second_or_term_started = 1;
            
		}
        
        if (c == '(') {
            // Add the previouos child elements to the current node and start a new lot of child elements
            if (second_or_term_started == 1) {
                logic_node *or_node = create_node();
                or_node->isOR = 1;
                or_node->children = first_child_node;

                current_node->next = or_node;
                current_node = current_child_node;
            } else {
                current_node->next = first_child_node;
                current_node = current_child_node;
            }
            
            first_child_node = NULL;
            first_child_node_set = 0;
        } else if (c == ')' && expression_has_finished == 0) {
			// Create the OR node that points to the right hand side inputs.
			logic_node *or_node = create_node();
			or_node->isOR = 1;
			or_node->children = first_child_node;
            
            // If c == ) we want to add the or node before we add it
			current_node->next = or_node;
			current_node = or_node;
            
            logic_node *bracketNode = create_node();
            bracketNode->name = c;
            current_node->next = bracketNode;
            current_node = bracketNode;
            
            second_or_term_started = 0;
            
            // If this is the first and the rest are just closing )'s we don't want to add more OR and brackets
            if (input[i+1] == ')') {
                expression_has_finished = 1;
            }
		} else if (c == '\n' && (current_node->isOR == 1)) {
            logic_node *or_node = create_node();
            or_node->isOR = 1;
            or_node->children = first_child_node;
            current_node->next = or_node;
            first_child_node_set = 0;
        }
		if (c == ')' || c == ' ') {
			// Reset first_child_node as we will be creating a new child if needed
			first_child_node_set = 0;
			first_child_node = NULL;
            current_child_node = NULL;
		}
	}
    
    // There is still a first child node we haen't sorted yet so add it to current
    if (first_child_node_set == 1) {
        current_node->next = first_child_node;
        current_node = first_child_node;
        first_child_node = NULL;
        first_child_node_set = 0;
    }
    
}

void printTree(logic_node *root_node) {
	logic_node *current_node = root_node;
	//printf("%c", root_node->name);
    // sometimes we lose the )'s at the end of expressions so we just need to add the ones that are missing
    int open_count = 0;
    int close_count = 0;
    
    int shouldCloseBracket = 0;
	while (current_node != NULL) {
        
		if (current_node->isOR == 1) {
            if (current_node->children->next != NULL) {
                printf("("); // Add extra bracket
                ++open_count;
                shouldCloseBracket = 1;
            }
            printTree(current_node->children);
            if (shouldCloseBracket == 1) {
                printf(")");
                ++close_count;
                shouldCloseBracket = 0;
            }
            
            if (current_node->next != NULL && current_node->next->name != ')') {
                printf(" OR ");
            }
		} else {
			if (current_node->isConstant == 1) {
				if (current_node->name == '1') {
					printf("TRUE");
				} else {
					printf("FALSE");
				}
			} else if (current_node->isNOT == 1) {
				printf("(NOT %c)", tolower(current_node->name));
			} else {
				printf("%c", current_node->name);
                if (current_node->name == '(') {
                    ++open_count;
                } else if (current_node->name == ')') {
                    ++close_count;
                }
			}
		}

		if (current_node->next != NULL) {
			if ((current_node->name != '(') && (current_node->next->name != ')') && (current_node->next->name != '\n') && (current_node->isOR == 0) && (current_node->next->isOR == 0)) {
				printf(" AND ");
			}
		}
        current_node = current_node->next;
	}
    
    // Check to make sure we have equal amounts of closing brackets
    if (open_count > close_count) {
        for (int i=0; i < (open_count-close_count); ++i) {
            printf(")");
        }
    }
}

logic_node *create_node() {
	logic_node *l = (logic_node *)malloc(sizeof(logic_node));
	// Just some initialization
	l->isOR = 0;
    l->isNOT = 0;
	return l;
}
